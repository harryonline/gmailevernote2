var versionSheet = SpreadsheetApp.getActive().getSheetByName('Versions');

/**
*  Check version with latest version, and send information email when update is available
*/
function checkVersion()
{
  if (!checkTime()) {
    return;
  }
  // Read latest version from Internet
  var latestVersion = getLatestVersion();
  var userVersion = getUserVersion();
  if( latestVersion.version <= userVersion ) {
    return;
  }
  saveVersionInfo(latestVersion);
  if (userVersion === 0 || !latestVersion.message) {
    return;
  }
  sendEmail('A new version of the Gmail to Evernote script has been released.', latestVersion.message);
}

/**
* Ensure version checking is called only once per day
*/
function checkTime() {
  var mSecDay = 86400000;
  var curDate = new Date();

  var nextCheck = new Date(getValue('NextCheck'));
  var doCheck = isNaN(nextCheck.getTime()) || curDate >= nextCheck;
  if (doCheck) {
    nextCheck = new Date(curDate.getTime() + mSecDay);
    setValue('NextCheck', nextCheck.toJSON().slice(0,16));
  }
  return doCheck;
}

function getLatestVersion() {
  var response = UrlFetchApp.fetch("https://gs.harryonline.net/gm2en2.json");
  return JSON.parse( response.getContentText() );
}

function getUserVersion() {
  var lastRow = versionSheet.getLastRow();
  var version = versionSheet.getRange(lastRow, 1).getValue();
  return typeof version === 'number' ? version : 0;
}

function saveVersionInfo(info) {
  versionSheet.appendRow([info.version, info.date, info.info]);
}

