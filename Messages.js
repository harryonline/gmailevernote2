/**
* Show message in browser alert or send as email
*/
function message(subject, body) {
  if (Browser && Browser.msgBox) {
    var ui = SpreadsheetApp.getUi();
    Browser.msgBox(subject, body, ui.ButtonSet.OK);
  } else {
    sendEmail(subject, body);
  }
}

/**
*  Send an email message to the user
*/
function sendEmail(subject, body) {
  var postText = "\n\nThis is an automated message from the Gmail to Evernote script.\nFor more information, visit http://www.harryonline.net/tag/gm2en";
  GmailApp.sendEmail(userMail, 'Gmail to Evernote: ' + subject, body + postText, { noReply: true });
}
