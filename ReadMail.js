// Global variables
var config;
var sentLabel;

/**
 * Read and process mail under Notebooklabel by threads
 */
function readMail() {
  checkVersion();
  config = readConfig();
  sentLabel = config.SentLabel && GmailApp.getUserLabelByName(config.SentLabel);
  var labels = getLabelTree(config.NotebookLabel, config.SentLabel);
  labels.forEach(function(label) {
    var threads = label.getThreads();
    threads.forEach(handleThread(label));
  });
}

/**
 * Send last email from thread to Evernote
 * @param {*} label by which the thread was selected
 */
function handleThread(label) {
  var path = label.getName().split('/');
  return function (thread) {
    var subject = getSubject(thread, path);
    var lastMessage = last(thread.getMessages());
    var body = getHeader(lastMessage) + getBody(lastMessage);
    GmailApp.sendEmail(config.EvernoteMail, subject, '', { htmlBody: body, attachments: lastMessage.getAttachments() });
    log(subject);
    if (sentLabel) {
      thread.addLabel(sentLabel);
    }
    if (!config.KeepSent) {
      removeMessage(subject);
    }
    thread.removeLabel(label);
    // Don't overload servers
    Utilities.sleep(1000);
  }
}

/**
 * Create subject line from original subject and notebook/tag labels
 * @param {*} thread 
 * @param {*} path 
 */
function getSubject(thread, path) {
  var subjectParts = [thread.getFirstMessageSubject()];
  // Add notebook if not default
  if (path.length > 1) {
    subjectParts.push('@' + last(path));
  }
  // Add default tag
  if (config.DefaultTag) {
    subjectParts.push('#' + config.DefaultTag);
  }
  // Add tags from other labels
  thread.getLabels().forEach(function (label) {
    var part = getSubjectPart(label);
    if (part) {
      subjectParts.push(part);
    }
  });
  return subjectParts.join(' ');
}

function getSubjectPart(label) {
  var name = label.getName();
  // Don't include the SentLabel
  if (name === config.SentLabel) {
    return '';
  }
  var labelParts = name.split('/');
  var top = labelParts[0];
  // Don't include any NotebookLabel or sublabel
  if (top === config.NotebookLabel) {
    return '';
  }
  // If Taglabel, don't include other labels
  if (config.TagLabel && top !== config.TagLabel) {
    return '';
  }
  return '#' + last(labelParts)
}

/**
 * Create header info for Evernote from email headers and links to open message in Gmail
 * @param {*} message 
 */
function getHeader(message) {
  return sprintf('<div style="%s">%s<br/>%s</div>', config.HeaderCss, emailHeaders(message), messageLinks(message));
}

function emailHeaders(message) {
  // Field that can be copied to Evernote
  var hdrFields = { bcc: 'Bcc', cc: 'Cc', date: 'Date', from: 'From', replyto: 'ReplyTo', subject: 'Subject', to: 'To' };
  var fields = config.EmailFields.toLowerCase().split(/[, ]+/);
  var headers = [];
  fields.forEach(function (field) {
    var fieldName = hdrFields[field];
    if (!fieldName) {
      return;
    }
    var funcName = 'get' + fieldName;
    var fieldValue = message[funcName]().toString();
    if (fieldValue) {
      headers.push(sprintf('%s: %s', fieldName, htmlEncode(fieldValue)));
    }
  })
  return headers.join('<br/>');
}

function messageLinks(message) {
  var nrAccounts = config.NrAccounts;
  if (!nrAccounts) {
    return '';
  }
  var links = [];
  for (var u = 0; u < nrAccounts; u++) {
    links.push(createLink(message.getId(), u));
  }
  return sprintf("%s (%s)", links.join(' - '), userMail);
}

/**
*  Create a HTML link to Gmail message
*  @param {string} msgId message ID
*  @param {string} user user no, 0 or higher if multiple sign-in
*  @return {string} url of GMail message
*/
function createLink(msgId, user) {
  var url = sprintf('https://mail.google.com/mail/u/%s/#inbox/%s', user, msgId);
  var text = user === 0 ? url : 'user ' + user;
  return sprintf('<a href="%s">%s</a>', url, text);
}

/**
*  Get message body, in pre-text if plain text
*/
function getBody(message) {
  var htmlBody = message.getBody()
  var plainBody = message.getPlainBody()
  if (htmlBody !== plainBody) {
    return htmlBody
  }
  return '<pre style="white-space:pre-wrap;font-family:monospace">' + plainBody + '</pre>'
}

/**
 * remove message just sent ot Evernote
 * @param {*} subject 
 */
function removeMessage(subject) {
  var sentResults = GmailApp.search("label:sent " + subject, 0, 1);
  if (sentResults.length > 0) {
    var sentMsg = last(sentResults[0].getMessages());
    if (sentMsg.getTo() == config.EvernoteMail) {
      sentMsg.moveToTrash();
    }
  }
}