/**
 * Handle configuration variables
 */
var variables = [
  {
    name: 'EvernoteMail',
    property: 'evernoteMail',
    defaultValue: '',
    required: true
  },
  {
    name: 'NotebookLabel',
    property: 'gm2en_nbk_label',
    defaultValue: 'Evernote',
    required: true
  },
  {
    name: 'TriggerEvery',
    defaultValue: 10,
    required: true
  },
  {
    name: 'DefaultTag',
    property: 'gm2en_tag',
    defaultValue: ''
  },
  {
    name: 'NrAccounts',
    property: 'gm2en_nacct',
    defaultValue: 1,
    required: true
  },
  {
    name: 'EmailFields',
    property: 'gm2en_fields',
    defaultValue: 'From, To, Cc, Date'
  },
  {
    name: 'HeaderCss',
    property: 'gm2en_hdrcss',
    defaultValue: 'border-bottom:1px solid #ccc;padding-bottom:1em;margin-bottom:1em;',
  },
  {
    name: 'TagLabel',
    property: 'gm2en_tag_label',
    defaultValue: '',
  },
  {
    name: 'SentLabel',
    property: 'gm2en_sent_label',
    defaultValue: '',
  },
  {
    name: 'KeepSent',
    property: 'gm2en_keep_sent',
    defaultValue: false,
  }
];

/**
 * Check if required variables are set, fill in default values
 */
function checkVariables() {
  var errors = [];
  variables.forEach(function (v) {
    var value = getValue(v.name);
    if (value) {
      return;
    }
    if (v.defaultVal) {
      setValue(v.name, v.defaultValue);
      return;
    }
    if (v.required) {
      errors.push(sprintf('%s: missing value', v.name));
    }
  });
  return errors;
}

/**
 * Batch read configuration from settings sheet
 */
function readConfig() {
  return variables.reduce(function (acc, v) {
    acc[v.name] = getValue(v.name);
    return acc;
  }, {});
}

