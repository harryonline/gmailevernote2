## Gmail to Evernote script

This script belongs to a spreadsheet, with which you can forward Gmail messages to Evernote based on Gmail labels. See http://googlescripts.harryonline.net/gmail-to-evernote for more details.

In the repository, you can browse the code without installing the script.

If you create a local copy of the script (using **clasp**, see https://github.com/google/clasp), you can set this repository as a remote and update the script without creating a new copy of the sheet. This would also allow you to make changes to the script and merge updates.
 