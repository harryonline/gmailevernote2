/**
*   Handle named ranges
*/
function getRangeByName(name) {
  return SpreadsheetApp.getActive().getRangeByName(name);
}

function getValue(name) {
  var range = getRangeByName(name);
  return range && range.getValue();
}

function setValue(name, value) {
  return getRangeByName(name).setValue(value);
}

/**
*  Simple alternative for php sprintf-like text replacements
*  Each '%s' in the format is replaced by an additional parameter
*  E.g. sprintf( '<a href="%s">%s</a>', url, text ) results in '<a href="url">text</a>'
*  @param {string} format text with '%s' as placeholders for extra parameters
*  @return {string} format with '%s' replaced by additional parameters
*/
function sprintf(format) {
  for (var i = 1; i < arguments.length; i++) {
    format = format.replace(/%s/, arguments[i]);
  }
  return format;
}

/**
*  Encode special HTML characters
*  From: http://jsperf.com/htmlencoderegex
*/
function htmlEncode(html) {
  return html.replace(/&/g, '&amp;').replace(/"/g, '&quot;').replace(/'/g, '&#39;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
}

/**
 * Return last element from array
 * From: https://github.com/lodash/lodash/blob/master/last.js
 * @param {array} array 
 */
function last(array) {
  const length = array == null ? 0 : array.length;
  return length ? array[length - 1] : undefined;
}

/**
 * Write subject to log sheet
 * @param {string} subject 
 */
function log(subject) {
  var logSheet = SpreadsheetApp.getActive().getSheetByName('Logs');
  logSheet.appendRow([new Date(), subject] );
}