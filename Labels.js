/**
 * Read all GMail labels and write them in Labels sheet
 */
function readLabels() {
  var allLabels = [];
  var topLevel = [];
  var allLabels = GmailApp.getUserLabels().map(function (label) {
    return label.getName();
  });
  allLabels.sort();
  var topLevel = allLabels.filter(function (name) { return name.indexOf('/') === -1 });
  fillColumn(topLevel, 1);
  fillColumn(allLabels, 2);
}

/**
 * Fill column in labels sheet with the given list
 * @param {*} list 
 * @param {*} i 
 */
function fillColumn(list, i) {
  var sheet = SpreadsheetApp.getActive().getSheetByName('Labels');
  sheet.getRange(1, i, sheet.getLastRow(), 1).clear();
  var range = sheet.getRange(1, i, list.length, 1)
  range.setValues(list.map(function (item) {
    return [item];
  }));
}

/**
 * Get topLabel and sublabels not equal to excludeLabel
 * @param {*} topLabel 
 * @param {*} excludeLabel 
 */
function getLabelTree(topLabel, excludeLabel) {
  var labels = GmailApp.getUserLabels();
  return labels.filter(function (label) {
    var name = label.getName();
    return name !== excludeLabel && name.split('/')[0] === topLabel;
  });
}

