var ENfunction = 'readMail';

/**
 * Get time triggers running the evernote function in this project
 */
function getTriggers() {
  var triggers = ScriptApp.getProjectTriggers();
  return triggers.filter(filterEvernote);
}

function filterEvernote(trigger) {
  var handlerFunction = trigger.getHandlerFunction();
  var eventType = trigger.getEventType();
  return handlerFunction === ENfunction && eventType === ScriptApp.EventType.CLOCK;
}

/**
 * Set trigger to run evernote function
 * @param {int} time in minutes
 */
function setTrigger(time) {
  stopTriggers();
  ScriptApp.newTrigger(ENfunction).timeBased().everyMinutes(time).create();
}

/**
 * Delete the time triggers
 */
function stopTriggers() {
  var triggers = getTriggers();
  triggers.forEach(deleteTrigger);
}

function deleteTrigger(trigger) {
  return ScriptApp.deleteTrigger(trigger);
}
