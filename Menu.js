// Global variables
var userMail = Session.getActiveUser().getEmail();

/**
 * Create menu and perform setup functions
 */
function onOpen() {
  var ui = SpreadsheetApp.getUi();
  ui.createMenu('Gmail to Evernote')
    .addItem('Start', 'startChecking')
    .addItem('Stop', 'stopChecking')
    .addItem('Check Now', 'readMail')
    .addItem('Read Labels', 'readLabels')
    .addToUi();
  checkStatus();
}

/**
 * Check variables, if OK set trigger to run script regularly
 */
function startChecking() {
  var errors = checkVariables();
  if (errors.length > 0) {
    message('Errors in Settings', errors.join('\n'));
    return;
  }
  var time = getValue('TriggerEvery');
  setTrigger(time);
  setStatus(true);
  message('Started', 'Your mail will be checked every ' + time + ' minutes. You can close the spreadsheet now.');
  readMail();
}

/**
 * Stop running the script
 */
function stopChecking() {
  stopTriggers();
  setStatus(false);
  message('Stopped', 'Your mail will not be checked anymore. You can close the spreadsheet now.');
}

/**
 * Check if any scripts are running, set status accordingly
 */
function checkStatus() {
  var triggers = getTriggers();
  setStatus(triggers.length > 0);
}

/**
 * Indicate in settings screen whether script is running
 * @param {*} running 
 */
function setStatus(running) {
  setValue('Status', running ? 'RUNNING' : 'STOPPED');
}
